## The long and the short of it

AD User management can be pretty easy or really complex, depending on the task. You can be working on a project that only involves moving users between OU's, such as in the case of ELHaynes where they have separate OU's for each grade level, or you could be modifying attributes en masse, such as department.

In some instances **Bulk AD Users** can be used if you feed it the apprpriate info. My experiences with it have been less than stellar, it was a little janky, but maybe things will run a bit smoother with an update. Once nice thing about it is that any changes you make using the program can be rolled back. I'm not going to detail how to use **Bulk AD Users** because it's pretty well documented by the [vendor](http://www.wisesoft.co.uk/software/bulkadusers/default.aspx). No need to reinvent the wheel. CSV import is the best way to to make mass modifications in my opinion, as a CSV is likely to have already been provided to us by the client, but there is a UI to navigate through AD, select users based on your filter criteria, and bulk edit attributes, change OU membership, etc.

PowerShell
==============

Now here is the real meat and potatoes. What sets PowerShell apart from programs like Bulk AD Users is that PowerShell is natively able to read an manipulate AD data by Microsoft design. You can obtain users by any criteria you can think of, native built-in support for the import and export of CSV's including tha ability to loop through them. Also, we can simulate the script before actually aplyying it to make sure that the script will do what we intend for it to do before we apply any updates.

Bulk AD Users uses the ADU&C interface via VBscripts to pull information and push changes. It only comes out to you in one format, it only accepts the same format going back in, and what it can pull/interact with is limited. PoweShell is much more flexible, allowing us to often times leverage client-supplied CSV's without needing to edit the CSV to make every field match, and we have access to any and all info we could want.

Here are some examples:

### Simple Example: Moving OU's

We're going to use the simplistic example of all of the students in a given school got promoted a grade level. The school already disabled the accounts of students who graduated and are no longer enrolled.

Let's assume the CSV Provided, users.csv, looks something like this:

| Name        |   Grade   |
|:-----------:|:---------:|
| Jane Doe    |    9th    |
| John Doe    |    10th   |
| Example Name| 11th      |
|Another One  | 12th      |

Add 300 more students and ignore some questionable naming choices, and we have ourselves a very straightforward example. And with this little information we can make magic happen.

```ps1
$Users = import-csv C:\users.csv
foreach ($user in $users){ select ($user.grade){
    
    9th{ get-aduser -filter $user.name | Move-ADObject -TargetPath 'OU=9th_grade,OU=Students,OU=Users,DC=contoso,DC=com'; break}
    
    10th{get-aduser -filter $user.name | Move-ADObject -TargetPath 'OU=10th_grade,OU=Students,OU=Users,DC=contoso,DC=com'; break}

    11th{get-aduser -filter $user.name | Move-ADObject -TargetPath 'OU=11th_grade,OU=Students,OU=Users,DC=contoso,DC=com'; break}
    
    12th{get-aduser -filter $user.name | Move-ADObject -TargetPath 'OU=12th_grade,OU=Students,OU=Users,DC=contoso,DC=com'; break}
}
}
```
With the little bit of information that was provided, we were able to leverage powershell to fill in the blanks and take approprate action depending on the grade level. If you're reading this far for fun and don't know much about PowerShell, learn it. Using any other tool would require a ton of time manually editing the customer-supplied CSV to exactly match AD attributes.

### A bit more complex: New Users, Department, and Security Group changes

Let's say a client went through a round of hiring, some people got promoted and therefore need group membership changes and department changes, and perhaps they decided to rename a department so we need to make those changes as well. We were given a list of new users, their user
