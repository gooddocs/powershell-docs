$PowerSchemes = (powercfg.exe /LIST) | Select-String "power scheme guid" -List
$AllowWakeTimersGUID = ((powercfg.exe /q) | Select-String "(Allow wake timers)").tostring().split(" ") | where {($_.length -eq 36) -and ([guid]$_)} 

foreach ($PowerScheme in $PowerSchemes) {
   $PowerSchemeGUID = $PowerScheme.tostring().split(" ") | where {($_.length -eq 36) -and ([guid]$_)}
   foreach ($Argument in ("/SETDCVALUEINDEX $PowerSchemeGUID SUB_SLEEP $AllowWakeTimersGUID 1","/SETACVALUEINDEX $PowerSchemeGUID SUB_SLEEP $AllowWakeTimersGUID 1")) {
    Start-Process powercfg.exe -ArgumentList $Argument -Wait -Verb runas -WindowStyle Hidden}}
