$NewUsers = import-csv C:\Powershell\NewUsers\Week1.csv | Select-Object -Property *,

@{name='GivenName';expression={$_."First Name"}},

@{name='Surname';expression={$_."Last name"}},

@{name='Name';expression={$_."Full name"}},

@{name='DisplayName';expression={$_."Full name"}},

@{name='EmployeeID';expression={$_."Employee ID"}},

@{name='SamAccountName';expression={$_."Username"}},

@{name='Path';expression={$_."Campus Assignment"}},

@{name='Company';expression={$_."Campus Assignment"}}

@{name='AccountPassword';expression={$_.Password}}

$NewUsers.AccountPassword = ConvertTo-SecureString $NewUsers.Password -AsPlainText -Force
$Domain = "OU='KIPP ACADEMY',DC=kippdc,DC=org"


ForEach ($User in $NewUsers){switch ($User.'Campus Assignment'){


    HQSchools {$User.Path = 'OU=School-Based,OU=HQ,',$domain; $User.ScriptPath = 'hq.bat'; $User.Company = 'HDQ-Schools'; break}

    PromiseStaff {$User.Path = 'OU=Staff,OU=Promise,',$domain; $User.ScriptPath = 'promise.bat'; $User.Company = 'Promise'; break}

    HQVAStaff {$User.Path = 'OU=VA Ave,OU=HQ,',$domain; $User.ScriptPath = 'hq.bat'; $User.Company = 'HDQ-K-Street'; break}

	AIMStaff {$User.Path = 'OU=Staff,OU=AIM,',$domain; $User.ScriptPath = 'aim.bat'; $User.Company = 'AIM'; break}

	ATAStaff {$User.Path = 'OU=Staff,OU=ATA,',$domain; $User.Company = 'ATA'; break}
	
	CollegePrepStaff {$User.Path = 'OU=Staff,OU="College Prep",',$domain; $User.ScriptPath = 'cp.bat'; $User.Company = 'College Prep'; break}

	ConnectStaff {$User.Path = 'OU=Staff,OU=Connect,',$domain; $User.ScriptPath = 'connect.bat'; $User.Company = 'Connect'; break}

	DiscoverStaff {$User.Path = 'OU=Staff,OU=Discover,',$domain; $User.ScriptPath = 'discover.bat'; $User.Company = 'Discover'; break}

	GrowStaff {$User.Path = 'OU=Staff,OU=Grow,',$domain; $User.ScriptPath = 'grow.bat'; $User.Company = 'Grow'; break}

	HeightsStaff {$User.Path = 'OU=Staff,OU=Heights,',$domain; $User.ScriptPath = 'Heights.bat'; $User.Company = 'Heights'; break}
	
	HQSpecialOktaEnabled {$User.Path = 'OU="Special Accounts - Okta Enabled",OU=HQ,',$domain; $User.ScriptPath = 'hq.bat'; $User.Company = 'HDQ-Schools'; break}
	
	HQContractor {$User.Path = 'OU=School-Based,OU=HQ,',$domain; $User.ScriptPath = 'hq.bat'; $User.Company = 'HDQ-Schools'; break}

	HQIntern {$User.Path = 'OU=School-Based,OU=HQ,',$domain; $User.ScriptPath = 'hq.bat'; $User.Company = 'HDQ-Schools'; break}

	KeyStaff {$User.Path = 'OU=Staff,OU=Key,',$domain; $User.ScriptPath = 'key.bat'; $User.Company = 'KEY'; break}

	KTCStaff {$User.Path = 'OU=Staff,OU=KTC,',$domain; $User.ScriptPath = 'ktc.bat'; $User.Company = 'KTC'; break}

	LeadStaff {$User.Path = 'OU=Staff,OU=Lead,',$domain; $User.ScriptPath = 'lead.bat'; $User.Company = 'Lead'; break}

	LeapStaff {$User.Path = 'OU=Staff,OU=LEAP,',$domain; $User.ScriptPath = 'leap.bat'; $User.Company = 'LEAP'; break}

	LearningCenter {$User.Path = 'OU=Staff,OU="Learning Center",',$domain; $User.ScriptPath = 'discover.bat.bat'; break}

	NortheastStaff {$User.Path = 'OU=Staff,OU=Northeast,',$domain; $User.Company = 'Northeast'; break}

	PromiseStaff {$User.Path = 'OU=Staff,OU=Promise,',$domain; $User.ScriptPath = 'Promise.bat'; $User.Company = 'Promise'; break}

	QuestStaff {$User.Path = 'OU=Staff,OU=Quest,',$domain; $User.Company = 'Quest'; break}

	SpringStaff {$User.Path = 'OU=Staff,OU=Spring,',$domain; $User.ScriptPath = 'Spring.bat'; $User.Company = 'Spring'; break}

	ValorStaff {$User.Path = 'OU=Staff,OU=Valor,',$domain; $User.ScriptPath = 'Valor.bat'; break}

	WillStaff {$User.Path = 'OU=Staff,OU=WILL,',$domain; $User.ScriptPath = 'WILL.bat'; $User.Company = 'WILL'; break}
}


ForEach ($User in $NewUsers){

New-ADUser `

-name $User.Name `

-displayname $User.displayname `

-GivenName $User.givenname `

-Surname $User.surname `

-EmailAddress $User.emailaddress `

-AccountPassword $User.AccountPassword `

-ChangePasswordAtLogon $True `

-EmployeeID $User.employeeID `

-Path $User.Path

-ScriptPath $User.ScriptPath

-SamAccountName $User.SamAccountName

-Company $User.Company

-Enabled $True
}


ForEach ($User in $NewUsers){switch ($User.'Campus Assignment'){

    HQSchools {add-adgroupmember -Identity '*HeadquartersAll-School-Based' -members $User.SamAccountName; break}

    PromiseStaff {add-adgroupmember -Identity '*Promise.ALL' -members $User.SamAccountName; break}

    HQVAStaff {add-adgroupmember -Identity '*HeadquartersAll-VA-Ave' -members $User.SamAccountName; break}
	
	AIMStaff {add-adgroupmember -Identity '*AIM.ALL' -members $User.SamAccountName; break}

	ATAStaff {add-adgroupmember -Identity '*ATA.ALL' -members $User.SamAccountName; break}

	CollegePrepStaff {add-adgroupmember -Identity '*CollegePrep.ALL' -members $User.SamAccountName; break}

	ConnectStaff {add-adgroupmember -Identity '*Connect.ALL' -members $User.SamAccountName; break}

	DiscoverStaff {add-adgroupmember -Identity '*Discover.ALL' -members $User.SamAccountName; break}

	GrowStaff {add-adgroupmember -Identity '*Grow.ALL' -members $User.SamAccountName; break}

	HeightsStaff {add-adgroupmember -Identity '*Heights.ALL' -members $User.SamAccountName; break}

	KeyStaff {add-adgroupmember -Identity '*KEY.ALL' -members $User.SamAccountName; break}

	KTCStaff {add-adgroupmember -Identity '*KIPPthroughCollege.ALL' -members $User.SamAccountName; break}

	LeadStaff {add-adgroupmember -Identity '*Lead.ALL' -members $User.SamAccountName; break}

	LeapStaff {add-adgroupmember -Identity '*LEAP.ALL' -members $User.SamAccountName; break}

	LearningCenter {add-adgroupmember -Identity '*LearningCenter.ALL' -members $User.SamAccountName; break}

	NortheastStaff {add-adgroupmember -Identity '*Northeast.ALL' -members $User.SamAccountName; break}

	QuestStaff {add-adgroupmember -Identity '*Quest.ALL' -members $User.SamAccountName; break}

	SpringStaff {add-adgroupmember -Identity '*Spring.ALL' -members $User.SamAccountName; break}

	ValorStaff {add-adgroupmember -Identity '*Valor.ALL' -members $User.SamAccountName; break}

	WillStaff {add-adgroupmember -Identity '*WILL.ALL' -members $User.SamAccountName; break}
	
	}
}

    }